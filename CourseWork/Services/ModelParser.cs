﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork.Services
{
    class ModelParser<T>
    {
        public static T LoadJson(string fileName)
        {
            using (StreamReader r = new StreamReader(fileName))
            {
                string json = r.ReadToEnd();
                T content = JsonConvert.DeserializeObject<T>(json);
                return content;
            }
        }

        public static T LoadJson(byte[] bArray)
        {
            using (var stream = new MemoryStream(bArray))
            using (var r = new StreamReader(stream, Encoding.UTF8))
            {
                string json = r.ReadToEnd();
                T content = JsonConvert.DeserializeObject<T>(json);
                return content;
            }
        }
        
        public static void SaveJson(string name, T value)
        {
            using (StreamWriter file = new StreamWriter(name, false))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, value);
            }
        }
    }
}
