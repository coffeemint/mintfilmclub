﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Media;
using System.Collections.ObjectModel;



namespace CourseWork.Models
{
    public class Film:IEquatable<object>
    {

        static int n = 1;
        public string ID { get; }
        public string Title { get; set; }
        public int Year { get; set; }
        public int Duration { get; set; }
        public ObservableCollection <Genre> Genres { get; set; }
        public string Director { get; set; }
        public Country Country { get; set; }
        public string Description { get; set; }
        public string TrailerPath { get; set; }
        public string ImagePath { get; set; }
        public ObservableCollection<Actor> Actors { get; set; }
        public ObservableCollection<string> ActorsID { get; set; }
        public bool IsLiked { get; set; }
        public bool IsWatched { get; set; }

        public string GenresString
        {
            get
            {
                if (Genres != null && Genres.Count > 0)
                {
                    string s = "";
                    foreach (Genre g in Genres)
                        s += ", " + g.ToString();
                    return s.Substring(2);
                }

                return "";
            }
            private set { }
        }

        public string ActorsString
        {
            get
            {
                if (Actors != null && Actors.Count > 0)
                {
                    string s = "";
                    foreach (Actor a in Actors)
                        s += ", " + a.Name;
                    return s.Substring(2);
                }
                return "";
            }
            private set { }
        }

        public Film()
        {
            ID = n.ToString();
            n++;
            IsLiked = false;
            IsWatched = false;
            Title = "";
            Year = 1980;
            Director = "";
            Description = "";
        }



        public Film (Film film)
        {
            ID = film.ID;
            Title = film.Title;
            Year = film.Year;
            Duration = film.Duration;
            if (film.Genres!=null)
            Genres = new ObservableCollection<Genre>(film.Genres);
            Director = film.Director;
            Country = film.Country;
            Description = film.Description;
            if (film.Actors != null)
            Actors = new ObservableCollection<Actor>(film.Actors);
            if (film.ActorsID != null)
            ActorsID = new ObservableCollection<string>(film.ActorsID);
            IsWatched = film.IsWatched;
            IsLiked = film.IsLiked;
            ImagePath = film.ImagePath;
            TrailerPath = film.TrailerPath;
        }

        public override string ToString()
        {            
                string genres = "";
            {
                if (this.Genres != null)

                    foreach (Genre g in this.Genres)
                    {
                        genres += g.ToString();
                        genres += ", ";
                    }
            }
            return Title + ", " + Year.ToString() + ", " + genres + Country.ToString();

        }


        public bool Validation()
        {
            if (Title != null && Director!=null)
            if (Title.Length > 2)
                if (Year >= 1980 && Year <= 2020)
                    if (Director.Length > 2)
                                    return true;
            return false;
        }

        public bool ContainActor(Actor actor)
        {
            foreach (Actor a in Actors)
            {
                if (a.ID == actor.ID)
                    return true;
            }
            return false;
    }

        public override bool Equals(object obj)
        {
            if (obj is Film)
                return this.ID == (obj as Film).ID;
            return base.Equals(obj);
        }


        public bool ContainGenre (object o)
        {
            foreach (Genre g in Genres)
            {
                if (o.ToString() == g.ToString())
                    return true;
            }
            return false;
        }

        public void AddGenre(object o)
        {
            foreach (Genre g in Enum.GetValues(typeof(Genre)).Cast<Genre>())
                if (g.ToString() == o.ToString())
                    Genres.Add(g);
        }


    }

   
}
