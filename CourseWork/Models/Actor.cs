﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork.Models
{
    public class Actor
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string ImagePath { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public Country Country { get; set; }
        public Gender Gender { get; set; }

        public string BirthDay
        {
            get { return Day.ToString() + "." + Month.ToString() + "." + Year.ToString(); }
            private set { }
        }


        public override string ToString()
        {

            return Name + " " + SecondName;
        }

        public Actor()
        {

        }

    }
}
