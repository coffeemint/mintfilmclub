﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork.Models
{
    public enum Genre
    {
        Romantic,
        Comedy,
        Fantastic,
        Fantasy,
        Adventure,
        Drama,
        Action,
        Horror,
        Detective,
        Thriller,
        Other
    }
}
