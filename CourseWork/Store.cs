﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using CourseWork.Models;

namespace CourseWork
{
    class Store
    {
        private const string filmFileName = "test.json";
        private const string actorFileName = "actors.json";

        private static Store instance;

        public ObservableCollection<int> Years { get; set; }

        public ObservableCollection<Film> Films { get; set; }
        public ObservableCollection<Actor> Actors { get; set; }

        public Film SelectedFilm { get; set; }
        public Actor SelectedActor { get; set; }

        public ObservableCollection<Film> LikedList
        {
            get { return GetLiked(); }
            set { }
        }

        public ObservableCollection<Film> WatchedList
        {
            get { return GetWatched(); }
            set { }
        }

        public bool AllowedToNavigate { get; set; }

        private Store()
        {
            Films = Services.ModelParser<ObservableCollection<Film>>.LoadJson(filmFileName);
            Actors = Services.ModelParser<ObservableCollection<Actor>>.LoadJson(actorFileName);
            SetActorsToFilms();
            Years = new ObservableCollection<int>();
            for (int i = 1980; i <= 2020; i++)
            {
                Years.Add(i);
            }
            AllowedToNavigate = true;

        }

        public static Store GetInstance()
        {
            if (instance == null)
                instance = new Store();

            return instance;
        }

        private ObservableCollection<Film> GetLiked()
        {
            IEnumerable<Film> films = Films.Where(f => (f.IsLiked == true));
            return new ObservableCollection<Film>(films);
        }

        private ObservableCollection<Film> GetWatched()
        {
            IEnumerable<Film> films = Films.Where(f => (f.IsWatched == true));
            return new ObservableCollection<Film>(films);
        }


        public void AddFilm(Film f)
        {
            Films.Add(f);
            SetSelectedFilm(f.ID);
        }

        public void SetActorsToFilms()
        {
            foreach (Film f in Films)
                if (f.ActorsID != null)
                    foreach (string actID in f.ActorsID)
                        foreach (Actor a in Actors)
                            if (a.ID == actID)
                            {
                                if (f.Actors == null)
                                    f.Actors = new ObservableCollection<Actor>();
                                if (!f.ContainActor(a))
                                     f.Actors.Add(a);
                            }
        }

        public void SetActorsIDByActors()
        {
            SelectedFilm.ActorsID = new ObservableCollection<string>();
            foreach (Actor a in SelectedFilm.Actors)
            {
                SelectedFilm.ActorsID.Add(a.ID);
            }
        }

        public bool CheckSearchByID(string id)
        {
            foreach (Film f in Films)
                if (f.ID == id)
                    return true;
            return false;
        }


        public void SetSelectedActor (string ID)
        {
            foreach (Actor a in Actors)
                if (a.ID == ID)
                    SelectedActor = a;
        }

        public void SetSelectedFilm(string ID)
        {
            if (GetFilmByID(ID)!=null)
            SelectedFilm = GetFilmByID(ID);

        }        

        public Film GetFilmByID(string ID)
        {
            foreach (Film f in Films)
                if (f.ID == ID)
                    return f;
            return null;
        }

        public Actor GetActorByID(string ID)
        {
            foreach (Actor a in Actors)
                if (a.ID == ID)
                    return a;
            return null;
        }

        public void DeleteFilm(string ID)
        {
            Films.Remove(GetFilmByID(ID));
        }

   
        public void Save()
        {
            Services.ModelParser<ObservableCollection<Film>>.SaveJson(filmFileName, Films);
            Services.ModelParser<ObservableCollection<Actor>>.SaveJson(actorFileName, Actors);

        }
    }
}
