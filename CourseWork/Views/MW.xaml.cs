﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CourseWork.Models;
using System.Collections.ObjectModel;


namespace CourseWork.Views
{
    /// <summary>
    /// Логика взаимодействия для MWindow.xaml
    /// </summary>
    public partial class MW : Window
    {
        Store store = Store.GetInstance();

        public MW()
        {        
            InitializeComponent();
            SearchListBox.SelectedValuePath = "ID";
            MainPage page = new MainPage(); 
            MyFrame.NavigationService.Navigate(page, UriKind.RelativeOrAbsolute);

            // MyFrame.NavigationService.Navigate(new System.Uri(@"Views\MainPage.xaml", UriKind.RelativeOrAbsolute));

        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            store.Save();
            Application.Current.Shutdown();


        }

        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
            ButtonCloseMenu.Visibility = Visibility.Visible;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonOpenMenu.Visibility = Visibility.Visible;
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
        }

        //private void SearchTextBoxChanged(object sender, TextChangedEventArgs e)
        //{
        //    SearchListBox.Visibility = Visibility.Visible;
        //    ObservableCollection<Film> fs = new ObservableCollection<Film>(store.Films);
        //    SearchListBox.ItemsSource = fs;
        //    // SearchListBox.DisplayMemberPath = "Title";
        //    foreach (Film f in store.Films)
        //        if (!f.Title.ToLower().Contains(SearchTextBox.Text.ToLower()))
        //        {
        //            fs.Remove(f);
        //        }
        //    if (SearchTextBox.Text == null || SearchTextBox.Text == "")
        //        SearchListBox.Visibility = Visibility.Collapsed;

        //}

        //private void SelectFilm(object sender, SelectionChangedEventArgs e)
        //{
        //    foreach (Film f in store.Films)
        //        if (f.ID == (SearchListBox.SelectedItem as Film).ID)
        //            Store.SelectedFilm = f;
        //    //Store.SelectedFilm = SearchListBox.SelectedItem as Film;
        //    MyFrame.NavigationService.Navigate(new Uri(@"Views\FilmPage.xaml", UriKind.RelativeOrAbsolute));
        //}

        private void SearchTextBoxChanged(object sender, TextChangedEventArgs e)
        {
            SearchListBox.Visibility = Visibility.Visible;
            ObservableCollection<Film> fs = new ObservableCollection<Film>(store.Films);
            SearchListBox.ItemsSource = fs;
            // SearchListBox.DisplayMemberPath = "Title";
            foreach (Film f in store.Films)
                if (!f.ToString().ToLower().Contains(SearchTextBox.Text.ToLower()))
                {
                    fs.Remove(f);
                }
            if (SearchTextBox.Text == null || SearchTextBox.Text == "")
                SearchListBox.Visibility = Visibility.Collapsed;

        }

        private void SelectFilm(object sender, SelectionChangedEventArgs e)
        {
            if (SearchListBox.SelectedValue != null)
            {

                store.SetSelectedFilm(SearchListBox.SelectedValue.ToString());
                FilmPage fPage = new FilmPage();
                MyFrame.NavigationService.Navigate(fPage);

                SearchTextBox.Text = null;
                SearchListBox.Visibility = Visibility.Collapsed;
                SearchListBox.SelectedValue = null;
            }
        }


        private void MyFrame_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {

        }


        private void SideMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            ButtonOpenMenu.Visibility = Visibility.Visible;
            ButtonCloseMenu.Visibility = Visibility.Collapsed;

            if (store.AllowedToNavigate)
            {
                if (HomeItem.IsSelected)
                {
                    MainPage page = new MainPage();
                    MyFrame.NavigationService.Navigate(page);
                }
                if (SearchItem.IsSelected)
                {
                    PickPage pickPage = new PickPage();
                    MyFrame.NavigationService.Navigate(pickPage);
                }

                if (LikedMenuItem.IsSelected)
                {

                    MainPage page = new MainPage(store.LikedList);
                    MyFrame.NavigationService.Navigate(page);
                }

                if (WatchedMenuItem.IsSelected)
                {

                    MainPage page = new MainPage(store.WatchedList);
                    MyFrame.NavigationService.Navigate(page);
                }
            }
            SearchItem.IsSelected = false;
            LikedMenuItem.IsSelected = false;
            WatchedMenuItem.IsSelected = false;
            HomeItem.IsSelected = false;
        }

  
            private void Window_MouseDown(object sender, MouseButtonEventArgs e)
            {
                if (e.ChangedButton == MouseButton.Left)
                    this.DragMove();
            }
        

        //private void Grid_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Back)
        //        if (store.AllowedToNavigate)
        //        {
        //            try
        //            {
        //                MyFrame.NavigationService.GoBack();
        //            }
        //            catch (System.InvalidOperationException) { }
        //        }
        //}
    }
}
