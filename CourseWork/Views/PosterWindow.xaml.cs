﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CourseWork.Views
{
    /// <summary>
    /// Логика взаимодействия для PosterWindow.xaml
    /// </summary>
    public partial class PosterWindow : Window
    {
        Store store = Store.GetInstance();
        public PosterWindow()
        {
            
            InitializeComponent();
            DataContext = store.SelectedFilm;
            Title = store.SelectedFilm.Title;
           
        }
    }
}
