﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CourseWork.Models;
using System.Collections.ObjectModel;

namespace CourseWork.Views
{
    /// <summary>
    /// Логика взаимодействия для Page.xaml
    /// </summary>
    public partial class MainPage : Page
    {

        public ObservableCollection<Film> Films { get; set; }
        //bool isLikedOrWatched = false;

        Store store = Store.GetInstance();
        public MainPage()
        {
            Films = store.Films;
            InitializeComponent();
            FilmView.ItemsSource = Films;
            FilmView.SelectedValuePath = "ID";
            FilmView.SelectedIndex = 0;



           DataContext = store.GetFilmByID(FilmView.SelectedValue.ToString());
        }

        public MainPage(ObservableCollection<Film> films) : this()
        {
            Films = films;
            FilmView.ItemsSource = Films;
            FilmView.SelectedIndex = 0;
            
        }


        private void FilmView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FilmView.SelectedValue != null)
            {
                DurationBox.DataContext = store.GetFilmByID(FilmView.SelectedValue.ToString());
                TitleBox.DataContext = store.GetFilmByID(FilmView.SelectedValue.ToString());
                YearBox.DataContext = store.GetFilmByID(FilmView.SelectedValue.ToString());
                GenreBox.DataContext = store.GetFilmByID(FilmView.SelectedValue.ToString());
                DirectorBox.DataContext = store.GetFilmByID(FilmView.SelectedValue.ToString());
                CountryBox.DataContext = store.GetFilmByID(FilmView.SelectedValue.ToString());
                Poster.DataContext = store.GetFilmByID(FilmView.SelectedValue.ToString());
          
            }

        }

        private void FilmView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
           
            FilmView.SelectedIndex = 0;

        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            store.DeleteFilm((FilmView.SelectedItem as Film).ID);
            FilmView.SelectedIndex = 0;
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            store.SetSelectedFilm((FilmView.SelectedItem as Film).ID);
            FilmPage page = new FilmPage(true);
            NavigationService.Navigate(page, UriKind.RelativeOrAbsolute);
        }

        private void Info_Click(object sender, RoutedEventArgs e)
        {
            ShowFilm();

        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Film f = new Film();
            store.AddFilm(f);
            FilmPage page = new FilmPage(true);
            NavigationService.Navigate(page);
        }

        private void FilmView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ShowFilm();

        }

        private void FilmView_KeyDown(object sender, KeyEventArgs e)
        {
                if (e.Key==Key.Enter)
                {
                    ShowFilm();
                }


            if (e.Key == Key.Up)
            {
                if (FilmView.SelectedIndex > 0)
                {
                    FilmView.SelectedItem = FilmView.Items[FilmView.SelectedIndex - 1];
                }
            }
            if (e.Key == Key.Down)
            {
                if (FilmView.SelectedIndex < FilmView.Items.Count - 1)
                {
                    FilmView.SelectedItem = FilmView.Items[FilmView.SelectedIndex + 1];
                }
            }

        }

        private void ShowFilm()
        {
            if (FilmView.SelectedIndex != -1)
            {
                store.SetSelectedFilm((FilmView.SelectedItem as Film).ID);

                FilmPage page = new FilmPage();
                NavigationService.Navigate(page);
            }
        }
    }
}
