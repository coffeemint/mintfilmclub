﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CourseWork.Models;
using System.Collections.ObjectModel;


namespace CourseWork.Views
{
 
    /// <summary>
    /// Логика взаимодействия для PickPage.xaml
    /// </summary>
    public partial class PickPage : Page
    {
        Store store = Store.GetInstance();
        ObservableCollection<Genre> Genres;
        ObservableCollection<Country> Countries;
        int minYear;
        int maxYear;
        public bool EachGenre { get; set; }
        IEnumerable<Film> films;

        public PickPage()
        {
            Genres = new ObservableCollection<Genre>();
            Countries = new ObservableCollection<Country>();
            InitializeComponent();

            CheckGenreList.ItemsSource = Enum.GetValues(typeof(Genre)).Cast<Genre>();
            CheckCountryList.ItemsSource = Enum.GetValues(typeof(Country)).Cast<Country>();
            FromBox.DataContext = store;
            TillBox.DataContext = store;
            NewFilms.SelectedValuePath = "ID";
            MustAllGenres.DataContext = this;

            FromBox.ItemsSource = store.Years;
            FromBox.SelectedIndex = 0;
            TillBox.ItemsSource = store.Years;
            TillBox.SelectedIndex = store.Years.Count - 1;
            
            
        }


        private void FromBoxMinYears_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            minYear = (int)FromBox.SelectedValue;
            this.Find();
        }

        private void TillBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            maxYear = (int)TillBox.SelectedValue;
            this.Find();
        }

        private void CheckGenreList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Genres = new ObservableCollection<Genre>();
            foreach (Genre g in CheckGenreList.SelectedItems)
                Genres.Add(g);
            this.Find();
        }

        private void CheckCountryList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Countries = new ObservableCollection<Country>();
            foreach (Country c in CheckCountryList.SelectedItems)
                    Countries.Add(c);
            this.Find();
        }

        private void Find()
        {
            IEnumerable<Film> films = store.Films.Where(f => (f.Year >= minYear && f.Year <= maxYear));
            if(Countries.Count > 0)
                films = films.Where(f => Countries.Contains(f.Country)).ToList();
            if(Genres.Count > 0)
                if(EachGenre)
                    films = films.Where(f => f.Genres.Where(g => Genres.Contains(g)).Count() == Genres.Count);
                else
                    films = films.Where(f => f.Genres.Where(g => Genres.Contains(g)).Count() > 0);
            
            NewFilms.ItemsSource = films;

        }

        private void MustAllGenres_Click(object sender, RoutedEventArgs e)
        {
            this.Find();
        }

        private void NewFilms_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OpenFilm();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFilm();
        }

        private void OpenFilm()
        {

            if (NewFilms.SelectedValue != null)
            {

                store.SetSelectedFilm(NewFilms.SelectedValue.ToString());
                FilmPage fPage = new FilmPage();
                NavigationService.Navigate(fPage);

                NewFilms.SelectedValue = null;
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (NewFilms.SelectedValue != null)
            {
                store.Films.Remove(store.GetFilmByID(NewFilms.SelectedValue.ToString()));
                this.Find();
            }
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            if (NewFilms.SelectedValue != null)
            {
                store.SetSelectedFilm(NewFilms.SelectedValue.ToString());
                FilmPage page = new FilmPage(true);
                NavigationService.Navigate(page);

            }
        }
    }
}
