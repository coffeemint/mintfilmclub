﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CourseWork.Models;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.IO;



namespace CourseWork.Views
{
    /// <summary>
    /// Логика взаимодействия для FilmPage.xaml
    /// </summary>
    public partial class FilmPage : Page
    {
        Store store = Store.GetInstance();
        Film film;
        Film initialFilm;
        public FilmPage()
        {
 
            film = store.SelectedFilm;
            InitializeComponent();

            DataContext = film;
            TitleBox.Text = film.Title.ToUpper();

            EditCountryBox.ItemsSource = Enum.GetValues(typeof(Country)).Cast<Country>();
            EditGenreBox.ItemsSource = Enum.GetValues(typeof(Genre)).Cast<Genre>();
            EditActorsBox.ItemsSource = store.Actors;
            EditYearBox.ItemsSource = store.Years;


            if (film.TrailerPath != "" && film.TrailerPath != null)
                TrailerBox.Source = new Uri(film.TrailerPath, UriKind.RelativeOrAbsolute);
            TrailerBox.LoadedBehavior = MediaState.Manual;
            TrailerBox.Stop();


            ActorBox.SelectedValuePath = "ID";


        
            DescriptionBox.IsReadOnly = true;
            TitleBox.IsReadOnly = true;
            DirectorBox.IsReadOnly = true;
            DurationBox.IsReadOnly = true;
            GenreBox.IsReadOnly = true;
            YearBox.IsReadOnly = true;
            CountryBox.IsReadOnly = true;

            PauseButton.Visibility = Visibility.Hidden;
            EditActorsBox.Visibility = Visibility.Hidden;
            EditCountryBox.Visibility = Visibility.Hidden;
            EditGenreBoxGrid.Visibility = Visibility.Hidden;
            ActorsStringBox.Visibility = Visibility.Hidden;
            EditYearBox.Visibility = Visibility.Hidden;

            CancelButton.Visibility = Visibility.Hidden;
            SaveButton.Visibility = Visibility.Hidden;
            LoadButton.Visibility = Visibility.Hidden;
            LoadVideoButton.Visibility = Visibility.Hidden;

            store.AllowedToNavigate = true;

            SetLikeColor();
            SetWatchedColor();
        }

        public FilmPage(bool isForEdit):this()
        {

            
            if (isForEdit)
            {

                initialFilm = new Film(film);
                DescriptionBox.IsReadOnly = false;
                TitleBox.IsReadOnly = false;
                DirectorBox.IsReadOnly = false;
                DurationBox.IsReadOnly = false;

                YearBox.Visibility = Visibility.Hidden;
                CountryBox.Visibility = Visibility.Hidden;
                ActorBox.Visibility = Visibility.Hidden;

                EditCountryBox.Visibility = Visibility.Visible;
                EditYearBox.Visibility = Visibility.Visible;
                ActorsStringBox.Visibility = Visibility.Visible;
                EditActorsBox.Visibility = Visibility.Visible;

                CancelButton.Visibility = Visibility.Visible;
                SaveButton.Visibility = Visibility.Visible;
                EditButton.Visibility = Visibility.Hidden;

                LoadButton.Visibility = Visibility.Visible;
                LoadVideoButton.Visibility = Visibility.Visible;
                PlayButton.Visibility = Visibility.Hidden;
                StopButton.Visibility = Visibility.Hidden;
                TimeSlider.Visibility = Visibility.Hidden;


                TextBoxSetColor(TitleBox, Brushes.DarkBlue);
                TextBoxSetColor(DirectorBox, Brushes.Black);
                EditYearBox.SelectedValue = film.Year;
                               
                EditActorsBox.SelectedItems.Clear();

                for (int i = 0; i < EditActorsBox.Items.Count; i++)
                {
                    Actor a = EditActorsBox.Items[i] as Actor;
                    if (film.ActorsID != null && film.ActorsID.Count > 0)
                        if (film.ActorsID.Contains(a.ID))
                        {
                            EditActorsBox.SelectedItems.Add(EditActorsBox.Items[i]);
                        }
                }

                ActorBoxSetColor();
                GenreBoxSetColor();

                store.AllowedToNavigate = false;
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {

            FilmPage page = new FilmPage(true);
            NavigationService.Navigate(page, UriKind.RelativeOrAbsolute);

        }


        private void ActorBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ActorBox.SelectedValue != null)
            {
                store.SetSelectedActor(ActorBox.SelectedValue.ToString());
                ActorPage actorPage = new ActorPage();
                NavigationService.Navigate(actorPage, UriKind.RelativeOrAbsolute);
            }
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PosterWindow w = new PosterWindow();
            w.Show();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            bool isOK = true;
            film.Year = (int)EditYearBox.SelectedValue;

            if (!TextBoxIsOk(DirectorBox))
            {
                isOK = false;
            }

            if (!DurationBoxIsOk())
            {
                isOK = false;
            }

            if (!ListViewIsOk(EditActorsBox))
            {
                isOK = false;
            }

            if (!ListViewIsOk(EditGenreBox) && (GenreBox.Text==null || GenreBox.Text==""))
            {
                isOK = false;
            }

            if (isOK)
            {
                store.SetActorsIDByActors();

                FilmPage page = new FilmPage();
                NavigationService.Navigate(page, UriKind.RelativeOrAbsolute);
            }

        }

        private bool ListViewIsOk (ListView box)
        {
            if (box.SelectedItems != null)
                if (box.SelectedItems.Count > 0)
                    return true;
            return false;
        }

        private bool ListViewIsOk(ListBox box)
        {
            if (box.SelectedItems != null)
                if (box.SelectedItems.Count > 0)
                    return true;
            return false;
        }

        private void DirectorBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBoxSetColor(DirectorBox, Brushes.Black);
        }

        private void DurationBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (!DurationBoxIsOk())
            {
                DurationBox.BorderBrush = Brushes.Red;
                DurationBox.Foreground = Brushes.DarkRed;
            }

            else
            {
                DurationBox.BorderBrush = null;
                DurationBox.Foreground = Brushes.Black;
            }
        }

        private bool TextBoxIsOk(TextBox box)
        {
            bool isOk = true;
            if (box.Text.Length < 2 || box.Text.Length > 100) 
                isOk = false;
            return isOk;

        }

        private bool DurationBoxIsOk()
        {
            bool isOk = true;
            int n = -1;
            Int32.TryParse(DurationBox.Text, out n);
            if (n < 1 || n > 1000)
            {
                isOk = false;
            }

            return isOk;
        }

        private void GenreBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (EditGenreBoxGrid.Visibility == Visibility.Visible)
                EditGenreBoxGrid.Visibility = Visibility.Hidden;
            else
                EditGenreBoxGrid.Visibility = Visibility.Visible;
            if (EditButton.Visibility == Visibility.Visible)
            {
                EditGenreBoxGrid.Visibility = Visibility.Hidden;
            }
      
        }

        private void ActorsBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            if (EditActorsBoxGrid.Visibility == Visibility.Visible)
                EditActorsBoxGrid.Visibility = Visibility.Hidden;
            else
                EditActorsBoxGrid.Visibility = Visibility.Visible;
            if (EditButton.Visibility == Visibility.Visible)
            {
                EditActorsBoxGrid.Visibility = Visibility.Hidden;
            }
        }



        private void EditGenreBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GenreBoxSetColor();

            if (film.Genres == null)
                film.Genres = new ObservableCollection<Genre>();

            film.Genres.Clear();
            foreach (object o in EditGenreBox.SelectedItems)
            {
                film.AddGenre(o);
            }
            GenreBox.Text = film.GenresString;
        }


        private void EditActorsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ActorBoxSetColor();
            
            film.Actors = new ObservableCollection<Actor>();
            foreach (object o in EditActorsBox.SelectedItems)
            {
                if (o!=null)
                film.Actors.Add((o as Actor));
               
            }

            ActorsStringBox.Text = film.ActorsString;
        }

        private void GenreBoxSetColor()
        {
            if ((GenreBox.Text == null || GenreBox.Text=="") && (EditGenreBox.SelectedItems==null || EditGenreBox.SelectedItems.Count==0))
            {
                GenreBox.Background = new SolidColorBrush(Colors.Red) { Opacity = 0.25 };
            }
            else
            {
                GenreBox.Background = Brushes.White;
            }
        }

        private void ActorBoxSetColor()
        {
            if (!ListViewIsOk(EditActorsBox))
            {
                ActorsStringBox.Background = new SolidColorBrush(Colors.Red) { Opacity = 0.25 };
            }
            else
            {
                ActorsStringBox.Background = Brushes.White;
            }
        }

        private void TitleBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBoxSetColor(TitleBox, Brushes.DarkBlue);
        }

        private void TextBoxSetColor (TextBox box, SolidColorBrush normalColor)
        {
            if (!TextBoxIsOk(box))
            {
                box.BorderBrush = Brushes.Red;
                box.Foreground = Brushes.DarkRed;
                box.Background = new SolidColorBrush(Colors.Red) { Opacity=0.25};
            }
            else
            {
                box.BorderBrush = null;
                box.Foreground = normalColor;
                box.Background = null;
            }
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg)|*.jpg|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                Console.WriteLine(openFileDialog.FileName);
                System.IO.Directory.CreateDirectory("Images");

                string str = DateTime.Now.ToString();
                str = str.Replace('.', '_');
                str = str.Replace(':', '_');
                str = str.Replace(' ', '_');

                string path = System.IO.Path.Combine("Images", film.Title + film.ID + str + System.IO.Path.GetExtension(openFileDialog.FileName));

                File.Copy(System.IO.Path.Combine(openFileDialog.FileName), path, true);

                film.ImagePath = path;

                ImageSourceConverter converter = new ImageSourceConverter();

                ImageBox.Source = (ImageSource)converter.ConvertFrom(film.ImagePath);
            }
        }

        private void LoadVideoButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Video files (*.mp4)|*.mp4|Video files (*.avi)|*.avi|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                Console.WriteLine(openFileDialog.FileName);
                System.IO.Directory.CreateDirectory("Trailers");


                string str = DateTime.Now.ToString();
                str = str.Replace('.', '_');
                str = str.Replace(':', '_');
                str = str.Replace(' ', '_');

                string path = System.IO.Path.Combine("Trailers", film.Title + film.ID + str + System.IO.Path.GetExtension(openFileDialog.FileName));

                File.Copy(System.IO.Path.Combine(openFileDialog.FileName), path, true  );

                    film.TrailerPath = path;
 
                    TrailerBox.Source = new Uri(film.TrailerPath, UriKind.Relative);

            }
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            TrailerBox.Play();
            PlayButton.Visibility = Visibility.Hidden;
            PauseButton.Visibility = Visibility.Visible;
        }

        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            TrailerBox.Pause();
            PlayButton.Visibility = Visibility.Visible;
            PauseButton.Visibility = Visibility.Hidden;
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            TrailerBox.Position = new TimeSpan(0);
            TimeSlider.Value = 0;
            TrailerBox.Stop();
            PlayButton.Visibility = Visibility.Visible;
            PauseButton.Visibility = Visibility.Hidden;
        }

        private void TimeSlider_LostMouseCapture(object sender, MouseEventArgs e)
        {
            TimeSpan time = new TimeSpan(0, 0, Convert.ToInt32(Math.Round(TimeSlider.Value)));
            TrailerBox.Position = time; 
        }

        private void TrailerBox_MediaOpened(object sender, RoutedEventArgs e)
        {
            TimeSlider.Maximum = TrailerBox.NaturalDuration.TimeSpan.TotalSeconds;

        }

        private void ButtonLike_Click(object sender, RoutedEventArgs e)
        {
            film.IsLiked = !film.IsLiked;
            SetLikeColor();
        }

        private void ButtonWatched_Click(object sender, RoutedEventArgs e)
        {
            film.IsWatched = !film.IsWatched;
            SetWatchedColor();
        }

        private void SetLikeColor()
        {
            if (film.IsLiked)
                ButtonLike.Foreground = new SolidColorBrush(Colors.Red) { Opacity = 1 };
            else
                ButtonLike.Foreground = new SolidColorBrush(Colors.DeepPink) { Opacity = 0.25 };
        }

        private void SetWatchedColor()
        {
            if (film.IsWatched)
                ButtonWatched.Foreground = new SolidColorBrush(Colors.Orange) { Opacity = 1 };
            else
                ButtonWatched.Foreground = new SolidColorBrush(Colors.DarkOrange) { Opacity = 0.25 };
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (!initialFilm.Validation())
            {
                if (store.CheckSearchByID(initialFilm.ID))
                {
                    store.Films.Remove(store.GetFilmByID(initialFilm.ID));
                    MainPage page = new MainPage();
                    NavigationService.Navigate(page);
                }

            }
            else
            {
                film = new Film(initialFilm);
                store.SelectedFilm = film;
                 FilmPage page = new FilmPage();
                NavigationService.Navigate(page);

            }
            store.AllowedToNavigate = true;
        }

        private void GenreBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            GenreBoxSetColor();
        }
    }
}
